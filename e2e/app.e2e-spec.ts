import { EventbookPage } from './app.po';

describe('eventbook App', () => {
  let page: EventbookPage;

  beforeEach(() => {
    page = new EventbookPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
