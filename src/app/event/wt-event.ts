/**
 *
 * (c) 2013-2017 Wishtack
 *
 * $Id: $
 */

export class WtEvent {

    static fromObject(data) {
        /* Think about using named parameters. */
        return new WtEvent(data.title, data.description);
    }

    constructor(
        public title?: string,
        public description?: string
    ) {}

}
