import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { WtEvent } from '../wt-event';

@Component({
    selector: 'wt-event-list',
    templateUrl: './event-list.component.html',
    styleUrls: ['./event-list.component.css']
})
export class EventListComponent implements OnInit {

    @Input() eventList: WtEvent[];
    @Output() onEventRemove = new EventEmitter<WtEvent>();

    constructor() {
    }

    ngOnInit() {
    }

    removeEvent(event: WtEvent) {
        this.onEventRemove.emit(event);
    }

}
