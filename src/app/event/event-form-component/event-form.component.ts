import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { WtEvent } from '../wt-event';

@Component({
    selector: 'wt-event-form',
    templateUrl: './event-form.component.html',
    styleUrls: ['./event-form.component.css']
})
export class EventFormComponent implements OnInit {

    @Output() onEventAdd = new EventEmitter<WtEvent>();

    event: WtEvent

    constructor() {
        this._resetEvent();
    }

    ngOnInit() {
    }

    addEvent() {
        this.onEventAdd.emit(this.event);
        this._resetEvent();
    }

    private _resetEvent() {
        this.event = new WtEvent();
    }

}
