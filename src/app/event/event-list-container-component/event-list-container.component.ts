/**
 *
 * (c) 2013-2017 Wishtack
 *
 * $Id: $
 */

import 'rxjs/add/operator/map';

import { WtEvent } from '../wt-event';
import { EventStore } from '../event-store';
import { Component } from '@angular/core';

@Component({
    selector: 'wt-event-list-container',
    templateUrl: './event-list-container.component.html'
})
export class EventListContainerComponent {

    eventList: WtEvent[];

    constructor(private _eventStore: EventStore) {
        this._retrieveEventList();
    }

    addEvent(event: WtEvent) {
        this._eventStore.addEvent(event);
        this._retrieveEventList();
    }

    removeEvent(event: WtEvent) {
        this._eventStore.removeEvent(event);
        this._retrieveEventList();
    }

    private _retrieveEventList() {
        this._eventStore.getEventList()
            .subscribe((eventList) => this.eventList = eventList);
    }

}
