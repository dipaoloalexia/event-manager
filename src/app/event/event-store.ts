import { WtEvent } from './wt-event';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
/**
 *
 * (c) 2013-2017 Wishtack
 *
 * $Id: $
 */

@Injectable()
export class EventStore {

    private _eventList: WtEvent[] = [];

    constructor(private _http: Http) {
    }

    addEvent(event: WtEvent) {
        this._eventList = [...this._eventList, event];
    }

    removeEvent(event: WtEvent) {
        this._eventList = this._eventList.filter((_event) => _event !== event);
    }

    getEventList(): Observable<WtEvent[]> {
        return this._http.get('http://localhost:8080/eventbook/api/events')
            .map((response) => {
                return response.json()
                    .map((data) => WtEvent.fromObject(data));
            });
    }

}
