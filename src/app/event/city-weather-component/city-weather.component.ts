import { Component, Input } from '@angular/core';

@Component({
    selector: 'wt-city-weather',
    templateUrl: './city-weather.component.html',
    styleUrls: ['./city-weather.component.css']
})
export class CityWeatherComponent {

    @Input() city: string;

    getTemperature(city) {
        return city.length * 5;
    }

}
